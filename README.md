# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Steps of installation:###
> Download and go to source folder
> import and run on an IDE or run the following command:
> *	javac main.java
> *	java main



###Features of the project### 
>1.  Gives Optimal answer with minimum number of coins required to do change from the cash register.
>2.  No brute force or greedy solution.
>3.  Program is not case sensitive. Eg : valid for show & SHOW.
>4.  Input argument type checking:
	* won't accpet negative numbers, Eg: put -1 -2 -3 -4 -5
	* won't accept more than 5 integer as parameter, Eg: take 1 1 1 1 1 1 1
	* error-proof against typo
	
	


### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact