import java.util.Scanner;

/*Main class to drive HelloChange program.*/
public class main {
	
	/*Main method to drive HelloChange program. */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		HelloChange CoinChange = new HelloChange();
		String choice ; //input argument
		String result;  
        String[] choiceWithParameters; // input arguments with parameters
		Scanner scan = new Scanner(System.in);
		System.out.println("ready");
        do
        {
            choice = scan.nextLine().toLowerCase();   
            choiceWithParameters = choice.split(" ");
            //sanitation checking for input
            boolean flag = true;
        	for(int i=1;i<choiceWithParameters.length;i++){		 	
        		try { 
        			if(Integer.parseInt(choiceWithParameters[i])<0)
        				flag=false;
        	        Integer.parseInt(choiceWithParameters[i]);
        	        
        	    } catch(NumberFormatException e) { 
        	        flag= false; 
        	    } catch(NullPointerException e) {
        	        flag= false;
        	    }
        	}
        	
            switch(choiceWithParameters[0])  
            {
	            case "show":
	            	if(choiceWithParameters.length!=1 ){
                		System.out.println("Wrong Argument!!!");
                		break;
                	}
	                result = CoinChange.show();
	                System.out.print(result);
	                break;
                case "put" :
                	if(choiceWithParameters.length>6 || flag== false){
                		System.out.println("Wrong Argument!!!");
                		break;
                	}
                	int[] bills =new int[5];
                	for(int i=1;i<choiceWithParameters.length;i++){
                		bills[i-1]=Integer.parseInt(choiceWithParameters[i]);
                	}
                	result = CoinChange.put(bills);
                    System.out.print( result);
                    break;
                    
                case "take" :
                	if(choiceWithParameters.length>6 || flag== false){
                		System.out.println("Wrong Argument!!!");
                		break;
                	}
                	int[] bill =new int[5];
                	for(int i=1;i<choiceWithParameters.length;i++){
                		bill[i-1]=Integer.parseInt(choiceWithParameters[i]);
                	}
                	result = CoinChange.take(bill);
                    System.out.print( result);
                    break;
                case "change":
                	if(choiceWithParameters.length!=2 || flag== false){
                		System.out.println("Wrong Argument!!!");
                		break;
                	}
                    result=CoinChange.makeChange(Integer.parseInt(choiceWithParameters[1]));
                    System.out.print(result);
                    break;
                case "quit" : 
                	if(choiceWithParameters.length!=1 ){
                		System.out.println("Wrong Argument!!!");
                		break;
                	}
                	System.out.println("bye");
                	System.exit(0);
                    break;
                default : System.out.print("Wrong Argument!!!");
                    break;
            }
            System.out.println();
            
        }while(choiceWithParameters[0] != "quit");  
        scan.close();
	}
}
