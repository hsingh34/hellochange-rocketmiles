import java.util.*;

/*The HelloChange program implements a programs that gives you methods to get put bills, take bills,
change bills and show bills. */
class HelloChange {
	
	
	static final int[] Coins={20,10,5,2,1};   //denomination of coins used (20,10,5,2,1)
	static int[] CashRegister = {1,2,3,4,5};  // initialized to values given in HelloChange-Rocketmiles.pdf
	
	/*Method to show the current state of cash register
	@return  string representing the current state of cash register.*/
	public String show(){
		return "$"+HelloChange.total() +" "+ Arrays.toString(CashRegister);
		
	}
	
	/*Method to put bills of each denomination in cash register.
	@param bills String array containing number of each bills.
	@return String representing the current state of cash register*/
	public  String put(int[] bills){
		for(int i = 0;i<CashRegister.length;i++){
			CashRegister[i]+=bills[i]; 
		}
		return "$"+HelloChange.total() +" "+ Arrays.toString(CashRegister);
		
	}
	
	/*Method take bills in each denomination and add it to cash register.
	@param bills String array containing number of each bills.
	@return String representing the current state of cash register*/
	public  String take(int[] bills){
		boolean flag = true;
		for(int i=0;i< CashRegister.length; i++){	
			if(CashRegister[i]< bills[i])
				flag=false;	
		}
		if(flag)
			for(int i=0;i<CashRegister.length;i++){
				CashRegister[i]-=bills[i];
			}
		return "$"+HelloChange.total() +" "+ Arrays.toString(CashRegister);
	}
	
	
	/*Method to show change in each denomination and remove from cash register.
	@param value amount whose change is required.(optimal solution to generate minimum number of bills).
	@return String representing bills that were used in changed value.*/
	public  String makeChange(int value){
		List<List<Integer>> result = new ArrayList<List<Integer>>();
	    ArrayList<Integer> curr = new ArrayList<Integer>();
	    int[] limit = CashRegister.clone();
	    makeChangeHelper(result,limit, curr , value);
	    int[] res = new  int[5];
	    if(!result.isEmpty()){
	    for(int i : (ArrayList<Integer>) result.get(0)){
	    	if(i==20) res[0]++;
	    	else if(i==10) res[1]++;
	    	else if(i==5) res[2]++;
	    	else if(i==2) res[3]++;
	    	else if(i==1) res[4]++;
	    	}
	    }
	    for(int i =0;i<res.length;i++){
	    	CashRegister[i]-=res[i];
	    }
	    return !result.isEmpty() ?Arrays.toString(res): "sorry";
	}
	
	/*Method to return total amount in cash register.
	@return integer value of total amount.*/
	public static int total(){
		int total=0;
		for(int i=0;i<CashRegister.length;i++){
			total+=CashRegister[i]*Coins[i];
		}
		return total;
	}
	
	/*Helper method to make change of given amount optimally. recursively finds the change with constraint on limit on number of coins of each denomination.
	@param result list of list to store all possible solution for the given amount.
	@param limit number of coins left of each denominations.
	@param curr list to temp values.
	@target target value to find the change  of.*/
	public static void makeChangeHelper(List<List<Integer>> result,int[] limit,ArrayList<Integer> curr, int target){
		if(target==0){
	        result.add(new ArrayList<Integer>(curr));
	        return;
	    }
		if(target<0)
			return;
		for(int i=0;i<limit.length;i++){
			if(limit[i]>0 && target -Coins[i] >=0){
				limit[i]--;
				curr.add(Coins[i]);
				makeChangeHelper(result, limit,curr,target-Coins[i]);
				curr.remove(curr.size()-1); 			
			}
		}
	}
}
